public class Crow{
	
	private boolean canFly;
	private int numFeathers;
	private String whatFood;
	
	public Crow(boolean response, int numberOfFeathers, String food){
		this.canFly = response;
		this.numFeathers = numberOfFeathers;
		this.whatFood = food;
		
	}
	
	

	public boolean getCanFly(){
		return this.canFly;
	}

	public int getNumFeathers(){
		return this.numFeathers;
	}
	public void setWhatFood(String food){
		
		this.whatFood = food;
		
	}
	public String getWhatFood(){
		return this.whatFood;
	}
	
	
	public void judgeFood(){
		if(!(this.whatFood.equals("steak"))){
			System.out.println("Rethink your diet");
		}else{
			System.out.println("Keep it up, lookin good! ;)");
		}	
	}
	
	public void noFlySad(){
		if(!canFly){
			System.out.println("You're a bird, what you mean you can't fly -  get yourself together");
		}else{
			System.out.println("Good.");
		}
	}
	
}